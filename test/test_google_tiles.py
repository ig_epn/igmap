from cartopy.io import img_tiles
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

margin = 0.50

longitud = -0.00
latitud = -77.77

zoom = 7
try:
    print("Intentando usar GoogleTiles...")
    tiles = img_tiles.GoogleTiles(style="street", cache=True)
except Exception as e:
    print(f"GoogleTiles failed: {e}")



print(tiles)


# Create the map and add the custom tiles
fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())  # Use Plate Carrée projection
ax.set_extent([longitud - margin, longitud + margin, latitud - margin, latitud + margin])

plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)

ax.add_image(tiles, zoom, interpolation='spline36', regrid_shape=1000)



plt.show()