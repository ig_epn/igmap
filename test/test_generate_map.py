


from os import sys, path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


print(sys.path)

from igmap_service import igmap_service_app as igms


##from igmap_service_app import generate_map


##INFORMACIÓN DE ENTRADA
config_file_path="/home/wacero/proyectos_codigo/igmap/data/igmap_config.cfg"
latitud = -1.55
longitud = -80.52
magnitud = 3.1
event_text = f"ID: igepn2016hnmu - Revisado \n 2016-04-16 18:58 Hora local \nMagnitud: {magnitud}"


igms.generate_map.generate_map(config_file_path,longitud,latitud,magnitud,event_text,'test.png')