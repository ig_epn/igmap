
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from igmap_service import igmap_service_app as igms

if __name__ == "__main__":

    colors = igms.get_circle_color.get_colors_from_magnitude(4.5)
    print(colors)

