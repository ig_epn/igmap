import os
import json

basedir = os.path.abspath(os.path.dirname(__file__))

with open("%s/data/igmap_info.json" %basedir,'r') as json_file:
    igmap_info = json.load(json_file)


class Config:

    SECRET_KEY = os.environ.get('SECRET_KEY')
    SERVICE_USER = igmap_info['user']
    IGMAP_TOKEN = igmap_info['igmap_token']
    IGMAP_CONFIG = igmap_info["igmap_config_path"]
    MAPBOX_ACCESS_TOKEN = igmap_info['mapbox_access_token'] or os.environ.get('ACCESS_TOKEN') 
    
    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True

class ProductionConfig(Config): 

    DEBUG = False

config = {
        'development': DevelopmentConfig,
        'production': ProductionConfig, 
        'default': DevelopmentConfig

            }