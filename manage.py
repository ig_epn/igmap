import os

from igmap_service import create_igmap_app

app = create_igmap_app(os.getenv('FLASK_CONFIG') or 'default')

if __name__ == '__main__':
    """
    Run the Flask application.

    This script starts the Flask web server. By default, the server will
    be accessible on 0.0.0.0:36002, allowing connections from any IP on 
    port 36002.
    """
    app.run(host='0.0.0.0', port=36002)

