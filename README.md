# IGMap

## Overview
IGMap is a Python service mapping application designed to create maps for SeisComP GDS modules (igtwitter, igfacebook, email, etc.)

## Features

- Customizable map layers and icons

## Getting Started

### Prerequisites
- Python 3.11
- Scipy 
- flask
- matplotlib-scalebar

### Installation
1. Clone the repository:

` git clone https://gitlab.com/ig_epn/igmap.git ` 



2. Navigate to the project directory:

` cd igmap ` 

3. Create the environment and install dependencies:

```
$ conda create -n igmap python=3.11 

$ conda activate igmap  

$ conda install cartopy scipy flask  

$ pip install matplotlib-scalebar 

```

4. Configure the service in `config.py`.

### Running the Application
1. Start the server:

`python manage.py runserver`

2. Create a map using the following url:

` wget "http://192.168.1.180:36002/create_igmap?event_id=igepn2023hnbmu&status=revisado&event_datetime=2023-01-01%2010:00&magnitude=3.7&latitud=4.47&longitud=-80.26" -O ./igmap.png`


## Service arquitecture

```mermaid
graph LR
    A[Client Request] -->|/create_igmap| B[views.py]
    B -->|Extract Parameters| C[Parameter Module]
    B -->|Generate Map| D[generate_map Module]
    C -->|Parameters| D
    D -->|Map Data| E[Map Rendering Module]
    E --> F[Map Image]
    F -->|Return Response| B
    B -->|Response| G[Client]

```

## Contributing
Contributions to IGMap are welcome! Please read our contributing guidelines for details on how to submit pull requests.

## License
This project is licensed under the [MIT License](LICENSE).

## Acknowledgments
- Wilson Armando Acero for recent contributions
- [Instituto Geofísico - EPN](https://www.igepn.edu.ec) for project support


