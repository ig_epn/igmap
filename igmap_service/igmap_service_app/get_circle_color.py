from scipy.interpolate import interp1d
import numpy as np

MAXIMUM_MAGNITUDE = 1.25
# Define the colors and corresponding intensity values
shakemap_values = np.linspace(1, 10, 10)  # Now goes from 1 to 10
shakemap_colors = [
    'rgb(128,128,255)', 'rgb(191,204,255)', 'rgb(160,230,255)', 'rgb(128,255,255)',
    'rgb(122,255,147)', 'rgb(255,255,0)', 'rgb(255,200,0)', 'rgb(255,145,0)',
    'rgb(230,0,0)', 'rgb(255,0,0)'
]

# Separate the RGB components for each color
shakemap_rgb = [tuple(map(int, color[4:-1].split(','))) for color in shakemap_colors]

# Create interpolation functions for each color component
interp_r = interp1d(shakemap_values, [color[0] / 255.0 for color in shakemap_rgb])
interp_g = interp1d(shakemap_values, [color[1] / 255.0 for color in shakemap_rgb])
interp_b = interp1d(shakemap_values, [color[2] / 255.0 for color in shakemap_rgb])

def get_colors_from_magnitude(magnitude):

    """
    Generates a list of color shades based on the given seismic event magnitude. 
    The function interpolates RGB color values within a predefined range and 
    adjusts the tones to create multiple shades.

    The magnitude is first adjusted by a maximum magnitude factor and then clamped 
    within the range [1, 10]. The function then interpolates the RGB values and 
    generates different shades by adjusting the lightness/darkness of the color.

    :param magnitude: The magnitude of the seismic event.
    :type magnitude: float
    :return: A list of tuples representing RGB color shades.
    :rtype: list of tuples

    :raises ValueError: If the magnitude is not a float or if it's out of the interpolation range after scaling.
    """
    
    # Clamp the magnitude to the range [1, 10]
    magnitude =  magnitude*MAXIMUM_MAGNITUDE
    magnitude = np.clip(magnitude, 1, 10)

    # Interpolate the color components
    r = interp_r(magnitude)
    g = interp_g(magnitude)
    b = interp_b(magnitude)
    
    # Generate 3 shades for each color
    # values lower than 1, dark the color
    # values greater than 1, light the color
    colors = []
    for factor in [0.6,0.8, 1.0, 1.3, 1.7]:
        r_tone = np.clip(r * factor, 0, 1)
        g_tone = np.clip(g * factor, 0, 1)
        b_tone = np.clip(b * factor, 0, 1)
        
        # Use a tuple for matplotlib RGB values
        colors.append((r_tone, g_tone, b_tone))
    
    return colors
