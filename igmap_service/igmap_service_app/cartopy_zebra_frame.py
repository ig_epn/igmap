import itertools
import matplotlib.patheffects as pe
import numpy as np

def add_zebra_frame(ax, lw=2, crs="pcarree", zorder=None):

    """
    Adds a zebra-styled frame around the provided axis of a matplotlib plot. 
    The frame consists of alternating black and white line segments.

    This function creates an aesthetically appealing border for geographic or other plots, 
    enhancing the visual distinction of the plot boundaries.

    :param ax: The matplotlib axis to which the zebra frame is added.
    :type ax: matplotlib.axes.Axes
    :param lw: Line width of the zebra stripes, defaults to 2.
    :type lw: int, optional
    :param crs: Coordinate reference system used for the transformation, defaults to "pcarree".
    :type crs: str, optional
    :param zorder: Z-order for plotting on the axis, can be None or a numerical value.
                  If None, the z-order is automatically determined.
    :type zorder: int or None, optional

    :raises ValueError: If `ax` is not a valid matplotlib Axes object.
    """    

    ax.spines["geo"].set_visible(False)
    left, right, bot, top = ax.get_extent()
    
    # Alternate black and white line segments
    bws = itertools.cycle(["k", "white"])

    xticks = sorted([left, *ax.get_xticks(), right])
    xticks = np.unique(np.array(xticks))
    yticks = sorted([bot, *ax.get_yticks(), top])
    yticks = np.unique(np.array(yticks))
    for ticks, which in zip([xticks, yticks], ["lon", "lat"]):
        for idx, (start, end) in enumerate(zip(ticks, ticks[1:])):
            bw = next(bws)
            if which == "lon":
                xs = [[start, end], [start, end]]
                ys = [[bot, bot], [top, top]]
            else:
                xs = [[left, left], [right, right]]
                ys = [[start, end], [start, end]]

            # For first and lastlines, used the "projecting" effect
            capstyle = "butt" if idx not in (0, len(ticks) - 2) else "projecting"
            for (xx, yy) in zip(xs, ys):
                ax.plot(
                    xx,
                    yy,
                    color=bw,
                    linewidth=lw,
                    clip_on=False,
                    transform=crs,
                    zorder=zorder,
                    solid_capstyle=capstyle,
                    # Add a black border to accentuate white segments
                    path_effects=[
                        pe.Stroke(linewidth=lw + 1, foreground="black"),
                        pe.Normal(),
                    ],
                )