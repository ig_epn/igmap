from flask import request, current_app, send_file 
import os
from . import igmap_app
from . import generate_map

@igmap_app.route('/create_igmap')
def call_igmap():
    """
    Handle the '/create_igmap' route in a Flask application. It generates a map based on seismic event data.

    This function extracts parameters from the request's query string, including latitude, longitude, 
    event ID, status, event datetime, magnitude, and depth. It then generates a map image representing 
    the seismic event and returns this image to the client.

    The function relies on the 'generate_map' module to create the map image.

    :return: A PNG image file of the generated map if successful, or an error message if not.
    :rtype: Flask Response object
    :raises ValueError: If latitude, longitude, or magnitude cannot be converted to float.
    :raises FileNotFoundError: If the generated image file is not found.
    """
    
    print("Start call_igmap()")
    # Obtener parámetros desde la URL
    config_file_path = current_app.config['IGMAP_CONFIG']
    
    latitud = float(request.args.get('latitud'))
    longitud = float(request.args.get('longitud'))
    event_id = request.args.get('event_id','')
    status = request.args.get('status','')
    event_datetime = request.args.get('event_datetime','')
    magnitude = float(request.args.get('magnitude'))
    depth = request.args.get('depth','')

    if depth != '':
        event_text = f"ID:{event_id} - {status} \n{event_datetime} Hora local \n Prof. {depth} Km. Magnitud: {magnitude}"
    else:
        event_text = f"ID:{event_id} - {status} \n{event_datetime} Hora local \n Magnitud: {magnitude}"


    event_image_file = f"/tmp/{event_id}.png"
    # Llamar a la función generate_map
    generate_map.generate_map(config_file_path, longitud, latitud, magnitude, event_text, event_image_file)

    if os.path.exists(event_image_file):
        return send_file(event_image_file, mimetype='image/png'),200
    else:
        return "No se pudo generar el mapa", 500 

