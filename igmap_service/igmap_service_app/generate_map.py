import configparser
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib_scalebar.scalebar import ScaleBar
from . import cartopy_zebra_frame as zebra
from . import  tiles_server
from . import get_circle_color

from cartopy.io import img_tiles
import cartopy.feature as cfeature

from matplotlib.offsetbox import OffsetImage, AnnotationBbox




def generate_map(config_file_path,longitud,latitud,magnitude,event_text, map_out_file):

    """
    Generates a map with seismic event data, including visual indicators like circles 
    representing seismic waves and a customized scale bar. The map is saved to a file.

    This function reads configuration settings from a file, sets up a map using 
    Cartopy and Matplotlib, and adds various layers and features based on the seismic 
    event's magnitude and location. The final map is saved as an image file.

    :param config_file_path: Path to the configuration file with settings for the map.
    :type config_file_path: str
    :param longitud: Longitude of the seismic event.
    :type longitud: float
    :param latitud: Latitude of the seismic event.
    :type latitud: float
    :param magnitude: Magnitude of the seismic event.
    :type magnitude: float
    :param event_text: Text to display on the map related to the event.
    :type event_text: str
    :param map_out_file: Path to the output file where the map will be saved.
    :type map_out_file: str

    :raises configparser.Error: If there is an issue reading the configuration file.
    :raises IOError: If there is an issue saving the map to the specified file.
    """




    print("Start generate_map()")

    ##Cargar la configuracion
    config = configparser.ConfigParser()
    config.read(config_file_path)
    # Acceder a los valores en la sección 'Settings'
    igepn_logo_file = config.get('Settings','igepn_logo_file')
    signal_file = config.get('Settings','signal_file')
    radios = [float(x) for x in config.get('Settings', 'radios').split(', ')]
    opacidades = [float(x) for x in config.get('Settings', 'opacidades').split(', ')]
    ##colors = config.get('Settings', 'colors').split(', ')
    colors = get_circle_color.get_colors_from_magnitude(magnitude)
    margin = float(config.get('Settings', 'margin'))
    inset_margin = int(config.get('Settings', 'inset_margin'))
    zoom = int(config.get('Settings', 'zoom'))
    figure_size = tuple(map(int, config.get('Settings', 'figure_size').split(', ')))
    logo_axes_array = [float(x) for x in config.get('Settings', 'logo_axes_array').split(', ')]
    map_tile_server_ip = config.get('Settings', 'map_tile_server_ip')
    map_tile_server_port = int(config.get('Settings', 'map_tile_server_port'))
    
    igmap_token = config.get('Settings', 'igmap_token')
    mapbox_access_token = config.get('Settings', 'mapbox_access_token')


    # Inicializar la variable 'tiles' a None
    tiles = None

    try:
        print("Intentando usar GoogleTiles...")
        tiles = img_tiles.GoogleTiles(style="street", cache=True)
    except Exception as e:
        print(f"GoogleTiles failed: {e}")


    if tiles is None:
        try:
            print("Intentando usar CustomTiles...")
            tiles = tiles_server.igmapServerTiles(map_tile_server_ip, map_tile_server_port,igmap_token) 
        
        except Exception as e:
            print(f"CustomTiles failed: {e}")


    if tiles is None:
        try:
            print("Intentando usar OSM...")
            #tiles = img_tiles.OSM()
        except Exception as e:
            print(f"OSM failed: {e}")

    if tiles is None:
        try:
            print("Intentando usar MapboxTiles...")
            tiles = tiles_server.MapboxTiles(mapbox_access_token)
        except Exception as e:
            print(f"MapboxTiles failed: {e}")

    # Comprobar si se pudo crear algún tile
    if tiles is not None:
        print("Tiles creados exitosamente.")
    else:
        print("Todos los métodos para crear tiles fallaron.")


    tiles_inset = tiles

    # Create the map and add the custom tiles
    fig = plt.figure(figsize=figure_size)
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())  # Use Plate Carrée projection
    ax.set_extent([longitud - margin, longitud + margin, latitud - margin, latitud + margin])

    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)

    ax.add_image(tiles, zoom, interpolation='spline36', regrid_shape=1000)
    ax.set_xticks([longitud- margin/2,longitud, longitud+margin/2], crs=ccrs.PlateCarree() )
    ax.set_yticks([latitud- margin/2,latitud, latitud+margin/2], crs=ccrs.PlateCarree() )
    ax.tick_params(axis='both', direction='in', which='both')

    # Add the circles
    for i, radio in enumerate(radios):
        circle = Circle((longitud, latitud), radius=radio, 
        facecolor='none',
        alpha=opacidades[i], 
        edgecolor=colors[i], 
        transform=ccrs.PlateCarree(),
        linewidth=1.5
        )
        ax.add_patch(circle)
    

    #'''
    # earthquake signal size
    ancho_icono = 0.07 
    alto_icono = 0.05 
    signal_extent = [longitud - ancho_icono / 2, longitud + ancho_icono / 2, latitud - alto_icono / 2, latitud + alto_icono / 2]

    signal_img = plt.imread(signal_file)
    # Coloca la imagen en el mapa en la ubicación del centro del círculo
    ax.imshow(signal_img, origin='lower', extent=signal_extent, transform=ccrs.PlateCarree(), zorder=10)
    #'''


    # Create the inset map and add the custom tiles
    inset_extent = [longitud-inset_margin, longitud+inset_margin, latitud-inset_margin, latitud+inset_margin]
    inset_ax = fig.add_axes([0.125, 0.125, 0.25, 0.25], projection=ccrs.PlateCarree())  # Use Plate Carrée projection
    inset_ax.set_extent(inset_extent)
    inset_ax.add_image(tiles_inset, 5, interpolation='spline36', regrid_shape=500)


    '''Generar el mapa con cfeatures aumenta varios segundos al proceso'''
    ###inset_ax.coastlines()
    ###inset_ax.add_feature(cfeature.BORDERS)
    ###inset_ax.add_feature(cfeature.OCEAN)
    inset_ax.plot(longitud, latitud, marker='+', color='red', markersize=10, transform=ccrs.PlateCarree())


    """Add the IGEPN logo to the map"""

    logo_img = plt.imread(igepn_logo_file)
    # Create an axes in the figure coordinates with the specified position and size.
    logo_axes = fig.add_axes(logo_axes_array)  # adjust as needed
    # Display the image in these axes
    logo_axes.imshow(logo_img)
    #logo_axes.axis('off')  # hide the axes and the white background
    logo_axes.tick_params(labelbottom=False, labelleft=False, bottom=False, left=False)



    """Add the text to the map"""
    up_text = event_text
    down_text = ""
    #ax.set_title(f"{up_text}")
    ax.text(x=0.95, y=0.95, s=f"{up_text}", size=13, ha='right', va='top', transform=ax.transAxes, bbox=dict(boxstyle="round,pad=0.3", fc="white", ec="black", lw=2))


    """Add the zebra scale on x and y axis"""
    zebra.add_zebra_frame(ax,crs=ccrs.PlateCarree())

    # Add a scale bar
    scalebar = ScaleBar(111, "km", length_fraction=0.20, location='lower right',box_alpha=0)
    ax.add_artist(scalebar)

    # Show the map
    #plt.show()
    plt.savefig(f'{map_out_file}')

    plt.close()
