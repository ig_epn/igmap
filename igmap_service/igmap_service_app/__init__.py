# Import the Blueprint class from flask module
from flask import Blueprint

# Create an instance of Blueprint named 'tile_server_app'
# '__name__' is used so that Flask knows where this Blueprint is defined
igmap_app = Blueprint('igmap_app',__name__)

# Import the views from the current package
# Views are the handlers that respond to the client requests
from . import views
