from cartopy.io import img_tiles
from cartopy.io.img_tiles import OSM

class MapboxTiles(OSM):
    """
    A custom tile provider class that fetches map tiles from Mapbox.

    This class extends the OSM class from Cartopy's img_tiles module. It overrides 
    the _image_url method to construct URLs that point to the Mapbox tile server, 
    using a provided access token.

    :param mapbox_access_token: Access token for Mapbox API. Defaults to "token".
    :type mapbox_access_token: str
    """

    def __init__(self,mapbox_access_token="token",*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.mapbox_access_token = mapbox_access_token

    def _image_url(self, tile,):
        """
        Construct the URL for a specific tile.

        :param tile: A tuple containing the x, y, and z coordinates of the tile.
        :type tile: tuple
        :return: A URL pointing to the tile image on the Mapbox server.
        :rtype: str
        """
        x, y, z = tile
        url = 'https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{}/{}/{}?access_token={}'.format(z, x, y, self.mapbox_access_token)
        return url


class igmapServerTiles(img_tiles.OSM):
    """
    A custom tile provider class that fetches map tiles from a custom server.

    This class extends the OSM class from Cartopy's img_tiles module. It overrides 
    the _image_url method to construct URLs that point to a custom map tile server, 
    using a provided IP address, port, and access token.

    :param server_ip: IP address of the custom server. Defaults to 'localhost'.
    :type server_ip: str
    :param server_port: Port number of the custom server. Defaults to 8080.
    :type server_port: int
    :param igmap_token: Access token for the custom server. Defaults to "clave".
    :type igmap_token: str
    """
    def __init__(self, server_ip='localhost', server_port=8080, igmap_token="clave", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.map_tile_server_ip = server_ip
        self.map_tile_server_port = server_port
        self.igmap_token = igmap_token
    def configure_server(self, server_ip, server_port):
        """
        Configure the IP address and port of the custom server.

        :param server_ip: IP address of the custom server.
        :type server_ip: str
        :param server_port: Port number of the custom server.
        :type server_port: int
        """
        self.map_tile_server_ip = server_ip
        self.map_tile_server_port = server_port
    
    def _image_url(self, tile):
        """
        Construct the URL for a specific tile.

        :param tile: A tuple containing the x, y, and z coordinates of the tile.
        :type tile: tuple
        :return: A URL pointing to the tile image on the custom server.
        :rtype: str
        """

        x, y, z = tile
        y = 2**z - y - 1
        url = f'http://{self.map_tile_server_ip}:{self.map_tile_server_port}/tiles?z={z}&x={x}&y={y}&token={self.igmap_token}'
        return url
