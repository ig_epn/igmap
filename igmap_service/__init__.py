from flask import Flask
from config import config

def create_igmap_app(config_name):
    """
    Create a Flask application using the specified configuration.

    This function creates a new Flask application and configures it using the
    configuration specified by `config_name`. The configuration is retrieved
    from the `config` dictionary imported from the `config` module. The function
    also initializes the application with the configuration using the `init_app`
    method of the configuration object.

    After the application is created and configured, the function registers the
    `main` blueprint with the application.

    Parameters
    ----------
    config_name : str
        The name of the configuration to use.

    Returns
    -------
    app : Flask
        The newly created and configured Flask application.
    """
    

    # Create a new Flask application
    app = Flask(__name__)

    # Configure the application using the specified configuration
    app.config.from_object(config[config_name])

    # Initialize the application with the configuration
    config[config_name].init_app(app)

    # Import the `main` blueprint and register it with the application
    from .igmap_service_app import igmap_app as main_blueprint
    app.register_blueprint(main_blueprint)
    
    # Return the newly created and configured Flask application
    return app

