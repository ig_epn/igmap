import sys,os
import configparser

print("Start of igmap_wsgi.py")

dir_path = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
config.read(os.path.join(dir_path, 'igmap_wsgi.config'))

# Obtener variables de configuración
HOME = config.get('Paths', 'HOME')
FLASK_CONFIG = config.get('Environment', 'FLASK_CONFIG')

sys.path.insert(0,dir_path)
os.environ['FLASK_CONFIG'] = FLASK_CONFIG

try:
    from igmap_service import create_igmap_app
    application = create_igmap_app(os.getenv('FLASK_CONFIG') or 'default')
    print("App igmap created ")
except Exception as e:
    print(f"Error while creating igmap app: {e}")
    
